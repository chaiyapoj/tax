@extends('layouts.master')
@section('title','สมัครสมาชิก')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item active" aria-current="page">สมัครสมาชิก</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="card mt-5">
                    <div class="card-header">
                        Register
                    </div>

                    <div class="card-body mx-auto col-7">
                        <h4 class="card-title">Register</h4>
                        <form method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="email">อีเมล์</label>
                                <input id="email" type="email" class="form-control" name="email" required="">
                            </div>

                            <div class="form-group">
                                <label for="password">รหัสผ่าน</label>
                                <input id="password" type="password" class="form-control" name="password" required="" data-eye="">
                            </div>

                            <div class="form-group">
                                <label for="password_confirmation">ยืนยันรหัสผ่าน</label>
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required="" data-eye="">
                            </div>

                            <div class="form-group no-margin">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Register
                                </button>
                            </div>
                            <div class="margin-top20 text-center">
                                หรือเคยสมัครแล้ว? <a href="/login">เข้าสู่ระบบ</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-center align-items-center">

        </div>
    </div>
@endsection

{{--
<form id="signup-form" action="/index.html" method="GET" novalidate="">
    <div class="form-group">
        <label for="firstName">ชื่อ-นามสกุล :</label>
        <div class="row">
            <div class="col-sm-6">
                <input type="text" class="form-control underlined" name="firstName" id="firstName" placeholder="กรอกชื่อ" required=""> </div>
            <div class="col-sm-6">
                <input type="text" class="form-control underlined" name="lastName" id="lastName" placeholder="กรอกนามสกุล" required=""> </div>
        </div>
    </div>
    <div class="form-group">
        <label for="email">อีเมล์ :</label>
        <input type="email" class="form-control underlined" name="email" id="email" placeholder="กรอกอีเมล์ของท่าน" required=""> </div>
    <div class="form-group">
        <label for="password">รหัสผ่าน :</label>
        <div class="row">
            <div class="col-sm-6">
                <input type="password" class="form-control underlined" name="password" id="password" placeholder="ตั้งรหัสผ่านของท่าน" required=""> </div>
            <div class="col-sm-6">
                <input type="password" class="form-control underlined" name="password_confirm" id="password_confirm" placeholder="ยืนยันรหัสผ่านของท่าน" required=""> </div>
        </div>
    </div>
    <div class="form-group">
        <label for="agree">
            <input class="checkbox" name="agree" id="agree" type="checkbox" required="">
            <span>Agree the terms and
                                        <a href="#">policy</a>
                                    </span>
         :</label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">สมัครสมาชิก</button>
    </div>
    <div class="form-group">
        <p class="text-muted text-center">เป็นสมาชิกอยู่แล้วหรือไม่ ?
            <a href="/login">เข้าสู่ระบบ</a>
        </p>
    </div>
</form>
--}}
