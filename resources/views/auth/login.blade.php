@extends('layouts.master')
@section('title','สมัครสมาชิก')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item active" aria-current="page">เข้าสู่ระบบ</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="card mt-5">
                    <div class="card-header">
                        เข้าสู่ระบบ
                    </div>

                    <div class="card-body mx-auto col-7">
                        <h4 class="card-title">ป้อนชื่อผู้ใช้และรหัสผ่านเพื่อเข้าสู่ระบบ</h4>
                        <form action="/login" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="email">อีเมล์</label>
                                <input id="email" type="text" class="form-control" name="email" required="">
                            </div>

                            <div class="form-group">
                                <label for="password">รหัสผ่าน</label>
                                <input id="password" type="password" class="form-control" name="password" required=""
                                       data-eye="">
                            </div>


                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="aggree" value="1"> จดจำรหัสผ่าน
                                </label>
                            </div>

                            <div class="form-group no-margin">
                                <button type="submit" class="btn btn-primary btn-block">
                                    เข้าสู่ระบบ
                                </button>
                            </div>
                            <div class="margin-top20 text-center">
                                หรือยังไม่เคยสมัคร? <a href="/register">สมัครสมาชิก</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-center align-items-center">

        </div>
    </div>
@endsection

