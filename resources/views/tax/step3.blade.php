@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="app" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li>เงินได้</li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">รายการลดหย่อนส่วนบุคคล</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">ค่าใช้จ่าย</label>
                                <div class="input-group">
                                    <input type="number"
                                           class="form-control" name="net_expenses" id="net_expenses"
                                           aria-describedby="net_expenses"
                                           placeholder=""
                                           @isset(session()->get('data')['net_expenses'])
                                           value="{{session()->get('data')['net_expenses']}}"
                                           @endisset
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>

                                </div>
                                <small id="net_expenses" class="form-text text-muted">ร้อยละ 50 แต่ไม่เกิน 100,000
                                    บาท
                                </small>
                            </div>
                            <div class="form-group">
                                <label for="">ลดหย่อนส่วนตัว</label>
                                <div class="input-group">
                                    <input type="number"
                                           class="form-control" name="personal_exemption"
                                           id="personal_exemption"
                                           aria-describedby="personal_exemption" placeholder=""
                                           @isset(session()->get('data')['personal_exemption'])
                                           value="{{session()->get('data')['personal_exemption']}}"
                                           @endisset
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                                <small id="personal_exemption" class="form-text text-muted">คนละ 60,000 บาท</small>
                            </div>
                            <div class="form-group">
                                <label for="">ลดหย่อนคู่สมรส</label>
                                <div class="input-group">
                                    <input type="number"
                                           class="form-control" name="spouse_allowance" id="spouse_allowance"
                                           aria-describedby="spouse_allowance" placeholder=""
                                           @isset(session()->get('data')['spouse_allowance'])
                                           value="{{session()->get('data')['spouse_allowance']}}"
                                           @endisset                                                 required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>

                                </div>
                                <small id="spouse_allowance" class="form-text text-muted">คนละ 60,000 บาท</small>
                            </div>
                            <div class="m-4">
                                <a href="/tax/step2">
                                    <button class="btn btn-secondary">ย้อนกลับ</button>
                                </a>
                                <button type="submit" class="btn btn-primary  btn-lg pull-right">ต่อไป</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>
        new Vue({
            el: '.app',
            data() {
                return {}
            }
        })
    </script>
@endpush

