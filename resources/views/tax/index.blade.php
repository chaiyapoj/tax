@extends('layouts.master')
@section('title','สถานะภาพของผู้มีเงินได้')
@section('content')
    <div class="container">
        <div class="row p-4">
            <div class="col-12">
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item active" aria-current="page">คำนวนภาษ</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <h5 class="card-header">Featured</h5>
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="/tax/step1" class="btn btn-primary">ข้าม</a>
                        <a href="/login" class="btn btn-secondary">เข้าสู่ระบบ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script>
        new Vue({
            el: '.app',
            data: {
                showModal: false
            }
        })
    </script>
@endpush