@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="app" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li>เงินได้</li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">รายการลดหย่อนส่วนบุคคล</h5>
                        <form action="" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="parental_care_allowance">อุปการะเลี้ยงดูบิดา-มารดาตนเอง</label>
                                <div class="input-group">
                                    <input type="number"
                                           class="form-control" name="parental_care_allowance"
                                           id="parental_care_allowance"
                                           aria-describedby="parental_care_allowance" placeholder=""
                                           @isset(session()->get('data')['parental_care_allowance'])
                                           value="{{session()->get('data')['parental_care_allowance']}}"
                                           @endisset
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                                <small id="parental_care_allowance" class="form-text text-muted">คนละ 30,000 บาท สูงสุด 2
                                    คน
                                </small>
                            </div>
                            <div class="form-group">
                                <label for="spouse_parental_care_allowance">อุปการะเลี้ยงดูบิดา-มารดาของคู่สมรส</label>
                                <div class="input-group">
                                    <input type="number"
                                           class="form-control" name="spouse_parental_care_allowance"
                                           id="spouse_parental_care_allowance"
                                           aria-describedby="spouse_parental_care_allowance" placeholder=""
                                           @isset(session()->get('data')['parental_care_allowance'])
                                           value="{{session()->get('data')['parental_care_allowance']}}"
                                           @endisset
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                            </div>
                            <small id="spouse_parental_care_allowance" class="form-text text-muted">คนละ 30,000 บาท
                                สูงสุด 2 คน
                            </small>
                            <div class="form-group">
                                <label for="disabled_care_allowance">อุปการะเลี้ยงดูคนพิการหรือคนทุพพลภาพ</label>
                                <div class="input-group">
                                    <input type="number"
                                           class="form-control" name="disabled_care_allowance"
                                           id="disabled_care_allowance"
                                           aria-describedby="disabled_care_allowance" placeholder=""
                                           @isset(session()->get('data')['disabled_care_allowance'])
                                           value="{{session()->get('data')['disabled_care_allowance']}}"
                                           @endisset
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                                <small id="disabled_care_allowance" class="form-text text-muted">สูงสุุด 1 คน 30,000
                                    บาท
                                </small>
                            </div>
                            <div class="m-4">
                                <a href="/tax/step3">
                                    <button class="btn btn-secondary">ย้อนกลับ</button>
                                </a>
                                <button type="submit" class="btn btn-primary  btn-lg pull-right">ต่อไป</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>

        @endsection

        @push('script')
            <script>
                new Vue({
                    el: '.app',
                    data() {
                        return {}
                    }
                })
            </script>
    @endpush

