@extends('layouts.master')
@section('title','สถานะภาพของผู้มีเงินได้')
@section('content')
    <div class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li class="active">เงินได้</li>
                    <li>รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">สถานะภาพของผู้มีเงินได้</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">สถานะภาพของผู้มีเงินได้</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="taxpayer_status" id="status.1" value="1"
                                           checked>
                                    โสด
                                </label>

                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="taxpayer_status" id="status.2" value="2">
                                    สมรส (ยื่นร่วม)
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="taxpayer_status" id="status.3" value="3">
                                    สมรส (แยกยื่น)
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="taxpayer_status" id="status.4" value="4">
                                    สมรส (แยกยื่น)
                                </label>
                            </div>
                            <div class="m-4">
                                <a href="/tax/step2">
                                    <button class="btn btn-secondary">ย้อนกลับ</button>
                                </a>
                                <button type="submit" class="btn btn-primary btn-lg pull-right">ต่อไป</button>
                            </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var vm = new Vue({
            el: '.app',
            data: {}
        })
    </script>
@endpush