@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li>เงินได้</li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">ค่าลดหย่อนอื่นๆ</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">เบี้ยประกันชีวิตของตนเอง</label>
                                        <div class="input-group">

                                            <input type="number"
                                                   class="form-control" name="reduce_self_life_insurance"
                                                   id="reduce_self_life_insurance"
                                                   aria-describedby="reduce_self_life_insurance" placeholder=""
                                                   @isset(session()->get('data')['reduce_self_life_insurance'])
                                                   value="{{session()->get('data')['reduce_self_life_insurance']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="reduce_self_life_insurance" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 100,000 บาท
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label for="">เบี้ยประกันชีวิตแบบบำนาญของตนเอง</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="reduce_pension_insurance"
                                                   id="reduce_pension_insurance"
                                                   aria-describedby="reduce_pension_insurance" placeholder=""
                                                   @isset(session()->get('data')['reduce_pension_insurance'])
                                                   value="{{session()->get('data')['reduce_pension_insurance']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <small id="reduce_pension_insurance" class="form-text text-muted">คิดจากเงินได้
                                        15%
                                        แต่ไม่เกิน 200,000 บาท
                                    </small>
                                    <div class="form-group">
                                        <label for="reduce_parents_health_insurance">เบี้ยประกันสุขภาพ บิดา-มารดา
                                            ของตนเอง</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="reduce_parents_health_insurance"
                                                   id="reduce_parents_health_insurance"
                                                   aria-describedby="reduce_parents_health_insurance" placeholder=""
                                                   @isset(session()->get('data')['reduce_parents_health_insurance'])
                                                   value="{{session()->get('data')['reduce_parents_health_insurance']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="reduce_parents_health_insurance" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 15,000 บาท ต่อคน
                                        </small>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="reduce_couple_life_insurance">เบี้ยประกันชีวิตของคู่สมรส</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="reduce_couple_life_insurance"
                                                   id="reduce_couple_life_insurance"
                                                   aria-describedby="reduce_couple_life_insurance" placeholder=""
                                                   @isset(session()->get('data')['reduce_couple_life_insurance'])
                                                   value="{{session()->get('data')['reduce_couple_life_insurance']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="reduce_couple_life_insurance" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 10,000 บาท
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label for="reduce_couple_pension_insurance">เบี้ยประกันชีวิตแบบบำนาญของคู่สมรส</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="reduce_couple_pension_insurance"
                                                   id="reduce_couple_pension_insurance"
                                                   aria-describedby="reduce_couple_pension_insurance" placeholder=""
                                                   @isset(session()->get('data')['reduce_couple_pension_insurance'])
                                                   value="{{session()->get('data')['reduce_couple_pension_insurance']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <small id="reduce_couple_pension_insurance" class="form-text text-muted">
                                        เท่าที่จ่ายจริง
                                        แต่ไม่เกิน 10,000 บาท
                                    </small>
                                </div>
                            </div>
                            <div class="m-4">
                                <a href="/tax/step4">
                                    <button class="btn btn-secondary">ย้อนกลับ</button>
                                </a>
                                <button type="submit" class="btn btn-primary  btn-lg pull-right">ต่อไป</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>

@endsection

{{--
        @push('script')
            <script>
                new Vue({
                    el: '.app',
                    data() {
                        return {}
                    }
                })
            </script>
    @endpush
--}}

