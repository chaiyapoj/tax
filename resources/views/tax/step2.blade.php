@extends('layouts.master')
@section('title','เงินได้')
@section('content')
    <div id="app" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li class="active">เงินได้</li>
                    <li>รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">เงินได้</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">เงินได้</h5>
                        <form action="" method="post">
                            @csrf

                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <input type="radio" name="income_type" id="income_type" value="monthly"
                                           v-model="income_type">
                                    เงินได้ต่อเดือน
                                </label>
                                <label class="checkbox-inline ml-4">
                                    <input type="radio" name="income_type" id="income_type" value="yearly"
                                           v-model="income_type">
                                    เงินได้ต่อปี
                                </label>
                            </div>
                            <div v-if="income_type === 'monthly'" class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="monthly_salary">เงินเดือน (บาท/เดือน)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="monthly_salary" id="monthly_salary"
                                                   aria-describedby="monthly_salary"
                                                   placeholder=""
                                                   @isset(session()->get('data')['monthly_salary'])
                                                   value="{{session()->get('data')['monthly_salary']}}"
                                                   @endisset                                                   required>
                                            <span class="input-group-append input-group-text">บาท / เดือน</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="yearly_bonus">โบนัส</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="yearly_bonus" id="yearly_bonus"
                                                   aria-describedby="yearly_bonus"
                                                   placeholder=""
                                                   @isset(session()->get('data')['yearly_bonus'])
                                                   value="{{session()->get('data')['yearly_bonus']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_dividend">เงินปันผล (บาท/เดือน)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="net_dividend" id="net_dividend"
                                                   aria-describedby="net_dividend"
                                                   placeholder=""
                                                   @isset(session()->get('data')['net_dividend'])
                                                   value="{{session()->get('data')['net_dividend']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_income">รายได้ทั้งหมด (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="net_income" id="net_income"
                                                   aria-describedby="net_income"
                                                   placeholder=""
                                                   @isset(session()->get('data')['net_income'])
                                                   value="{{session()->get('data')['net_income']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_holding_tax">ภาษีเงินได้ หัก ณ ที่จ่าย (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="net_holding_tax"
                                                   id="net_holding_tax"
                                                   aria-describedby="net_holding_tax"
                                                   placeholder=""
                                                   @isset(session()->get('data')['net_holding_tax'])
                                                   value="{{session()->get('data')['net_holding_tax']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="income_type === 'yearly'" class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">เงินเดือน (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="net_salary" id="net_salary"

                                                   aria-describedby="net_salary" placeholder=""
                                                   @isset(session()->get('data')['net_salary'])
                                                   value="{{session()->get('data')['net_salary']}}"
                                                   @endisset                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_income">รายได้ทั้งหมด (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="net_income"
                                                   id="net_income"
                                                   aria-describedby="net_income"
                                                   placeholder=""
                                                   value=""
                                                   @isset(session()->get('data')['net_income'])
                                                   value="{{session()->get('data')['net_income']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_holding_tax">ภาษีเงินได้ หัก ณ ที่จ่าย (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="net_holding_tax"
                                                   id="net_holding_tax"
                                                   aria-describedby="tax_deducted"
                                                   placeholder=""
                                                   @isset(session()->get('data')['net_holding_tax'])
                                                   value="{{session()->get('data')['net_holding_tax']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-4">
                                <a href="/tax/step1">
                                    <button class="btn btn-secondary">ย้อนกลับ</button>
                                </a>
                                <button type="submit" class="btn btn-primary  btn-lg pull-right">ต่อไป</button>
                            </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        new Vue({
            el: '.app',
            data() {
                return {
                    income_type: 'monthly'
                }
            }
        })
    </script>
@endpush

