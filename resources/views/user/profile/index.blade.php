@extends('layouts.master')
@section('title','สถานะภาพของผู้มีเงินได้')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item active" aria-current="page">สมัครสมาชิกใหม่</li>
        </ol>
    </nav>
@stop