@extends('layouts.master')
@section('title','เงินได้พึงประเมิน')
@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">สมาชิก</li>
            <li class="breadcrumb-item">ข้อมูลส่วนตัว</li>
            <li class="breadcrumb-item active" aria-current="page">ข้อมูลผู้พิการหรือทุพพลภาพ</li>
        </ol>
    </nav>
    {{--    {{dump(Auth::user()->Taxpayer->Child)}}--}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <div class="pull-left">
                            <h5 class="card-title">ข้อมูลผู้พิการหรือทุพพลภาพ</h5>

                        </div>
                        @if(Auth::user()->Taxpayer->Child()->count()<3)
                            <div class="pull-right">
                                <a href="/user/profile/disabled/create">
                                    <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>
                                        เพิ่มผู้พิการหรือทุพพลภาพ
                                    </button>
                                </a>
                            </div>
                        @endif
                        @if(count($disableds))
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ลำดับที่</th>
                                    <th>ชื่อ-สกุล</th>
                                    <th>วันเกิด</th>
                                    <th>ประเภท</th>
                                    <th>จัดการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($disableds as $disabled)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$disabled->pre_name}} {{$disabled->first_name}} {{$disabled->last_name}}</td>
                                        <td>{{$disabled->birth_date}}</td>
                                        <td>{{$disabled->type}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/user/profile/disabled/{{$disabled->id}}">
                                                    <button type="button" data-toggle="tooltip" title="แก้ไข"
                                                            class="btn btn-primary btn-sm"><i class="fa fa-edit">
                                                            แก้ไข</i>
                                                    </button>
                                                </a>
                                                <a href="/user/profile/disabled/{{$disabled->id}}/delete">
                                                    <button data-toggle="tooltip" title="ลบ" type="button"
                                                            class="btn btn-danger btn-sm"><i
                                                                class="fa fa-trash-o"></i> ลบ
                                                    </button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endisset

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
