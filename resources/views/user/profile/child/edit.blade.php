@extends('layouts.master')
@section('title','ข้อมูลบุตร')
@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">หน้าหลัก</a></li>
            <li class="breadcrumb-item"><a href="/user/profile/taxpayer">ข้อมูลส่วนตัว</a></li>
            <li class="breadcrumb-item"><a href="/user/profile/child">ข้อมูลบุตร</a></li>
            <li class="breadcrumb-item active" aria-current="page">เพิ่มบุตร</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <form action="" method="post">
                            @csrf
                            <h5 class="card-title">แก้ไขบุตร</h5>

                            <div class="form-group">
                                <label for="pre_name">คำนำหน้าชื่อ</label>
                                <select class="form-control" name="pre_name" id="pre_name">
                                    <option value="นาย" {{ ($child->pre_name='นาย'?'checked':'') }}>นาย</option>
                                    <option value="นาง" {{ ($child->pre_name='นาย'?'checked':'') }}">นาง</option>
                                    <option value="น.ส." {{ ($child->pre_name='นาย'?'checked':'') }}>นางสาว</option>
                                    <option value="พล.ต.อ." {{ ($child->pre_name='นาย'?'checked':'') }}>พลตำรวจเอก
                                    </option>
                                    <option value="พล.ต.อ. หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>พลตำรวจเอก
                                        หญิง
                                    </option>
                                    <option value="พล.ต.ท" {{ ($child->pre_name='นาย'?'checked':'') }}>พลตำรวจโท
                                    </option>
                                    <option value="พล.ต.ท หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>พลตำรวจโท
                                        หญิง
                                    </option>
                                    <option value="พล.ต.ต" {{ ($child->pre_name='นาย'?'checked':'') }}>พลตำรวจตรี
                                    </option>
                                    <option value="พล.ต.ต หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>พลตำรวจตรี
                                        หญิง
                                    </option>
                                    <option value="พ.ต.อ." {{ ($child->pre_name='นาย'?'checked':'') }}>พันตำรวจเอก
                                    </option>
                                    <option value="พ.ต.อ. หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>พันตำรวจเอก
                                        หญิง
                                    </option>
                                    <option value="พ.ต.อ.(พิเศษ)" {{ ($child->pre_name='นาย'?'checked':'') }}>
                                        พันตำรวจเอกพิเศษ
                                    </option>
                                    <option value="พ.ต.อ.(พิเศษ) หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>
                                        พันตำรวจเอกพิเศษ หญิง
                                    </option>
                                    <option value="พ.ต.ท." {{ ($child->pre_name='นาย'?'checked':'') }}>พันตำรวจโท
                                    </option>
                                    <option value="พ.ต.ท. หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>พันตำรวจโท
                                        หญิง
                                    </option>
                                    <option value="พ.ต.ต." {{ ($child->pre_name='นาย'?'checked':'') }}>พันตำรวจตรี
                                    </option>
                                    <option value="พ.ต.ต. หญิง" {{ ($child->pre_name='นาย'?'checked':'') }}>พันตำรวจตรี
                                        หญิง
                                    </option>
                                    <option value="ร.ต.อ.">ร้อยตำรวจเอก</option>
                                    <option value="ร.ต.อ. หญิง">ร้อยตำรวจเอก หญิง</option>
                                    <option value="ร.ต.ท.">ร้อยตำรวจโท</option>
                                    <option value="ร.ต.ท. หญิง">ร้อยตำรวจโท หญิง</option>
                                    <option value="ร.ต.ต.">ร้อยตำรวจตรี</option>
                                    <option value="ร.ต.ต. หญิง">ร้อยตำรวจตรี หญิง</option>
                                    <option value="ด.ต.">นายดาบตำรวจ</option>
                                    <option value="ด.ต. หญิง">ดาบตำรวจหญิง</option>
                                    <option value="ส.ต.อ.">สิบตำรวจเอก</option>
                                    <option value="ส.ต.อ. หญิง">สิบตำรวจเอก หญิง</option>
                                    <option value="ส.ต.ท.">สิบตำรวจโท</option>
                                    <option value="ส.ต.ท. หญิง">สิบตำรวจโท หญิง</option>
                                    <option value="ส.ต.ต.">สิบตำรวจตรี</option>
                                    <option value="ส.ต.ต. หญิง">สิบตำรวจตรี หญิง</option>
                                    <option value="จ.ส.ต.">จ่าสิบตำรวจ</option>
                                    <option value="จ.ส.ต. หญิง">จ่าสิบตำรวจ หญิง</option>
                                    <option value="พลฯ">พลตำรวจ</option>
                                    <option value="พลฯ หญิง">พลตำรวจ หญิง</option>
                                    <option value="พล.อ.">พลเอก</option>
                                    <option value="พล.อ. หญิง">พลเอก หญิง</option>
                                    <option value="พล.ท.">พลโท</option>
                                    <option value="พล.ท. หญิง">พลโท หญิง</option>
                                    <option value="พล.ต.">พลตรี</option>
                                    <option value="พล.ต.หญิง">พลตรี หญิง</option>
                                    <option value="พ.อ.">พันเอก</option>
                                    <option value="พ.อ.หญิง">พันเอก หญิง</option>
                                    <option value="พ.อ.(พิเศษ)">พันเอกพิเศษ</option>
                                    <option value="พ.อ.(พิเศษ) หญิง">พันเอกพิเศษ หญิง</option>
                                    <option value="พ.ท.">พันโท</option>
                                    <option value="พ.ท.หญิง">พันโท หญิง</option>
                                    <option value="พ.ต.">พันตรี</option>
                                    <option value="พ.ต.หญิง">พันตรี หญิง</option>
                                    <option value="ร.อ.">ร้อยเอก</option>
                                    <option value="ร.อ.หญิง">ร้อยเอก หญิง</option>
                                    <option value="ร.ท.">ร้อยโท</option>
                                    <option value="ร.ท.หญิง">ร้อยโท หญิง</option>
                                    <option value="ร.ต.">ร้อยตรี</option>
                                    <option value="ร.ต.หญิง">ร้อยตรี หญิง</option>
                                    <option value="ส.อ.">สิบเอก</option>
                                    <option value="ส.อ.หญิง">สิบเอก หญิง</option>
                                    <option value="ส.ท.">สิบโท</option>
                                    <option value="ส.ท.หญิง">สิบโท หญิง</option>
                                    <option value="ส.ต.">สิบตรี</option>
                                    <option value="ส.ต.หญิง">สิบตรี หญิง</option>
                                    <option value="จ.ส.อ.">จ่าสิบเอก</option>
                                    <option value="จ.ส.อ.หญิง">จ่าสิบเอก หญิง</option>
                                    <option value="จ.ส.ท.">จ่าสิบโท</option>
                                    <option value="จ.ส.ท.หญิง">จ่าสิบโท หญิง</option>
                                    <option value="จ.ส.ต.">จ่าสิบตรี</option>
                                    <option value="จ.ส.ต.หญิง">จ่าสิบตรี หญิง</option>
                                    <option value="พลฯ">พลทหารบก</option>
                                    <option value="ว่าที่ พ.ต.">ว่าที่ พ.ต.</option>
                                    <option value="ว่าที่ พ.ต. หญิง">ว่าที่ พ.ต. หญิง</option>
                                    <option value="ว่าที่ ร.อ.">ว่าที่ ร.อ.</option>
                                    <option value="ว่าที่ ร.อ. หญิง">ว่าที่ ร.อ. หญิง</option>
                                    <option value="ว่าที่ ร.ท.">ว่าที่ ร.ท.</option>
                                    <option value="ว่าที่ ร.ท. หญิง">ว่าที่ ร.ท. หญิง</option>
                                    <option value="ว่าที่ ร.ต.">ว่าที่ ร.ต.</option>
                                    <option value="ว่าที่ ร.ต. หญิง">ว่าที่ ร.ต. หญิง</option>
                                    <option value="พล.ร.อ.">พลเรือเอก</option>
                                    <option value="พล.ร.อ.หญิง">พลเรือเอก หญิง</option>
                                    <option value="พล.ร.ท.">พลเรือโท</option>
                                    <option value="พล.ร.ท.หญิง">พลเรือโท หญิง</option>
                                    <option value="พล.ร.ต.">พลเรือตรี</option>
                                    <option value="พล.ร.ต.หญิง">พลเรือตรี หญิง</option>
                                    <option value="น.อ.">นาวาเอก</option>
                                    <option value="น.อ.หญิง">นาวาเอก หญิง</option>
                                    <option value="น.อ.(พิเศษ)">นาวาเอกพิเศษ</option>
                                    <option value="น.อ.(พิเศษ) หญิง">นาวาเอกพิเศษ หญิง</option>
                                    <option value="น.ท.">นาวาโท</option>
                                    <option value="น.ท.หญิง">นาวาโท หญิง</option>
                                    <option value="น.ต.">นาวาตรี</option>
                                    <option value="น.ต.หญิง">นาวาตรี หญิง</option>
                                    <option value="ร.อ.">เรือเอก</option>
                                    <option value="ร.อ.หญิง">เรือเอก หญิง</option>
                                    <option value="ร.ท.">เรือโท</option>
                                    <option value="ร.ท.หญิง">เรือโท หญิง</option>
                                    <option value="ร.ต.">เรือตรี</option>
                                    <option value="ร.ต.หญิง">เรือตรี หญิง</option>
                                    <option value="พ.จ.อ.">พันจ่าเอก</option>
                                    <option value="พ.จ.อ.หญิง">พันจ่าเอก หญิง</option>
                                    <option value="พ.จ.ท.">พันจ่าโท</option>
                                    <option value="พ.จ.ท.หญิง">พันจ่าโท หญิง</option>
                                    <option value="พ.จ.ต.">พันจ่าตรี</option>
                                    <option value="พ.จ.ต.หญิง">พันจ่าตรี หญิง</option>
                                    <option value="จ.อ.">จ่าเอก</option>
                                    <option value="จ.อ.หญิง">จ่าเอก หญิง</option>
                                    <option value="จ.ท.">จ่าโท</option>
                                    <option value="จ.ท.หญิง">จ่าโท หญิง</option>
                                    <option value="จ.ต.">จ่าตรี</option>
                                    <option value="จ.ต.หญิง">จ่าตรี หญิง</option>
                                    <option value="พลฯ">พลทหารเรือ</option>
                                    <option value="พล.อ.อ.">พลอากาศเอก</option>
                                    <option value="พล.อ.อ.หญิง">พลอากาศเอก หญิง</option>
                                    <option value="พล.อ.ท.">พลอากาศโท</option>
                                    <option value="พล.อ.ท.หญิง">พลอากาศโท หญิง</option>
                                    <option value="พล.อ.ต.">พลอากาศตรี</option>
                                    <option value="พล.อ.ต.หญิง">พลอากาศตรี หญิง</option>
                                    <option value="น.อ.">นาวาอากาศเอก</option>
                                    <option value="น.อ.หญิง">นาวาอากาศเอก หญิง</option>
                                    <option value="น.อ.(พิเศษ)">นาวาอากาศเอกพิเศษ</option>
                                    <option value="น.อ.(พิเศษ) หญิง">นาวาอากาศเอกพิเศษ หญิง</option>
                                    <option value="น.ท.">นาวาอากาศโท</option>
                                    <option value="น.ท.หญิง">นาวาอากาศโท หญิง</option>
                                    <option value="น.ต.">นาวาอากาศตรี</option>
                                    <option value="น.ต.หญิง">นาวาอากาศตรี หญิง</option>
                                    <option value="ร.อ.">เรืออากาศเอก</option>
                                    <option value="ร.อ.หญิง">เรืออากาศเอก หญิง</option>
                                    <option value="ร.ท.">เรืออากาศโท</option>
                                    <option value="ร.ท.หญิง">เรืออากาศโท หญิง</option>
                                    <option value="ร.ต.">เรืออากาศตรี</option>
                                    <option value="ร.ต.หญิง">เรืออากาศตรี หญิง</option>
                                    <option value="พ.อ.อ.">พันจ่าอากาศเอก</option>
                                    <option value="พ.อ.อ.หญิง">พันจ่าอากาศเอก หญิง</option>
                                    <option value="พ.อ.ท.">พันจ่าอากาศโท</option>
                                    <option value="พ.อ.ท.หญิง">พันจ่าอากาศโท หญิง</option>
                                    <option value="พ.อ.ต.">พันจ่าอากาศตรี</option>
                                    <option value="พ.อ.ต.หญิง">พันจ่าอากาศตรี หญิง</option>
                                    <option value="จ.อ.">จ่าอากาศเอก</option>
                                    <option value="จ.อ.หญิง">จ่าอากาศเอก หญิง</option>
                                    <option value="จ.ท.">จ่าอากาศโท</option>
                                    <option value="จ.ท.หญิง">จ่าอากาศโท หญิง</option>
                                    <option value="จ.ต.">จ่าอากาศตรี</option>
                                    <option value="จ.ต.หญิง">จ่าอากาศตรี หญิง</option>
                                    <option value="พลฯ">พลทหารอากาศ</option>
                                    <option value="หม่อม">หม่อม</option>
                                    <option value="ม.จ.">หม่อมเจ้า</option>
                                    <option value="ม.ร.ว.">หม่อมราชวงศ์</option>
                                    <option value="ม.ล.">หม่อมหลวง</option>
                                    <option value="ดร.">ดร.</option>
                                    <option value="ศ.ดร.">ศ.ดร.</option>
                                    <option value="ศ.">ศ.</option>
                                    <option value="ผศ.ดร.">ผศ.ดร.</option>
                                    <option value="ผศ.">ผศ.</option>
                                    <option value="รศ.ดร.">รศ.ดร.</option>
                                    <option value="รศ.">รศ.</option>
                                    <option value="นพ.">นพ.</option>
                                    <option value="พญ.">แพทย์หญิง</option>
                                    <option value="นสพ.">สัตวแพทย์</option>
                                    <option value="สพญ.">สพญ.</option>
                                    <option value="ทพ.">ทพ.</option>
                                    <option value="ทพญ.">ทพญ.</option>
                                    <option value="ภก.">เภสัชกร</option>
                                    <option value="ภกญ.">ภกญ.</option>
                                    <option value="พระ">พระ</option>
                                    <option value="พระครู">พระครู</option>
                                    <option value="พระมหา">พระมหา</option>
                                    <option value="พระสมุห์">พระสมุห์</option>
                                    <option value="พระอธิการ">พระอธิการ</option>
                                    <option value="สามเณร">สามเณร</option>
                                    <option value="แม่ชี">แม่ชี</option>
                                    <option value="บาทหลวง">บาทหลวง</option>
                                    <option value="Mr.">MR</option>
                                    <option value="Mrs. ">MRS</option>
                                    <option value="Ms. ">MS</option>
                                    <option value="Miss">MISS</option>
                                    <option value="Dr.">Dr.</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="first_name">ชื่อ</label>
                                <input id="first_name"
                                       type="name"
                                       class="form-control" name="first_name"
                                       value="{{$child->first_name}}"
                                       aria-describedby="first_name" placeholder="" required/>
                            </div>
                            <div class="form-group">
                                <label for="last_name">นามสกุล</label>
                                <input id="last_name"
                                       type="name"
                                       class="form-control" name="last_name"
                                       value="{{$child->last_name}}"
                                       aria-describedby="last_name" placeholder="" required/>
                            </div>
                            <div class="form-group">
                                <label for="birth_date">วันเกิด</label>
                                <input id="birth_date"
                                       type="date"
                                       class="form-control" name="birth_date"
                                       value="{{$child->birth_date}}"

                                       aria-describedby="birth_date" placeholder="" required/>
                            </div>
                            <div class="form-group">
                                <label for="type">ประเภท</label>
                                <div id="type" class="input-group">
                                    <label class="checkbox-inline mr-2">
                                        <input type="radio" name="type" id="0"
                                               value="0" {{ ($child->type=0?'checked':'') }}>
                                        บุตรแท้
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" name="type" id="1"
                                               value="1" {{ ($child->type=1?'checked':'') }}>
                                        บุตรบุญธรรม
                                    </label>
                                </div>

                                <button type="submit" class="btn btn-primary btn-lg w-25 mt-4">บันทึก</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection