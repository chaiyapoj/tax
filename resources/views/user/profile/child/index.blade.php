@extends('layouts.master')
@section('title','เงินได้พึงประเมิน')
@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">สมาชิก</li>
            <li class="breadcrumb-item">ข้อมูลส่วนตัว</li>
            <li class="breadcrumb-item active" aria-current="page">ข้อมูลบุตร</li>
        </ol>
    </nav>
    {{--    {{dump(Auth::user()->Taxpayer->Child)}}--}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <div class="pull-left">
                            <h5 class="card-title">ข้อมูลบุตร</h5>

                        </div>
                        @if(Auth::user()->Taxpayer->Child()->count()<3)
                            <div class="pull-right">
                                <a href="/user/profile/child/create">
                                    <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>
                                        เพิ่มบุตร
                                    </button>
                                </a>
                            </div>
                        @endif
                        @if(count($children))
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ลำดับที่</th>
                                    <th>ชื่อ-สกุล</th>
                                    <th>วันเกิด</th>
                                    <th>ประเภท</th>
                                    <th>จัดการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($children as $child)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$child->pre_name}} {{$child->first_name}} {{$child->last_name}}</td>
                                        <td>{{$child->birth_date}}</td>
                                        <td>{{$child->type}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/user/profile/child/{{$child->id}}">
                                                    <button type="button" data-toggle="tooltip" title="แก้ไข"
                                                            class="btn btn-primary btn-sm"><i class="fa fa-edit">
                                                            แก้ไข</i>
                                                    </button>
                                                </a>
                                                <a href="/user/profile/child/{{$child->id}}/delete">
                                                    <button data-toggle="tooltip" title="ลบ" type="button"
                                                            class="btn btn-danger btn-sm"><i
                                                                class="fa fa-trash-o"></i> ลบ
                                                    </button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endisset

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
