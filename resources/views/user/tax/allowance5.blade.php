@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li><a href="/user/tax/assessable_income">เงินได้</a></li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">ค่าลดหย่อนอื่นๆ</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="spouse_parental_health_insurance_premium_allowance">กองทุนประกันสังคมของคู่สมรส</label>
                                        <div class="input-group">

                                            <input type="number"
                                                   min="0"
                                                   max="9000"
                                                   class="form-control" name="spouse_social_security_fund_contribution"
                                                   id="spouse_social_security_fund_contribution"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['spouse_social_security_fund_contribution'])
                                                   value="{{session()->get('allowances')['spouse_social_security_fund_contribution']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="spouse_social_security_fund_contribution"
                                               class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 9,000 บาท
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label for="retirement_mutual_fund">กองทุนรวมเพื่อการเลี้ยงชีพ (RMF)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="500000"
                                                   class="form-control" name="retirement_mutual_fund"
                                                   id="retirement_mutual_fund"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['retirement_mutual_fund'])
                                                   value="{{session()->get('allowances')['retirement_mutual_fund']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <small id="retirement_mutual_fund" class="form-text text-muted">คิดจากเงินได้ 15%
                                        แต่ไม่เกิน 500,000 บาท
                                    </small>
                                    <div class="form-group">
                                        <label for="long_term_equity_fund">กองทุนรวมหุ้นระยะยาว (LTF)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="60000"
                                                   class="form-control" name="long_term_equity_fund"
                                                   id="long_term_equity_fund"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['long_term_equity_fund'])
                                                   value="{{session()->get('allowances')['long_term_equity_fund']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="long_term_equity_fund" class="form-text text-muted">
                                            คิดจากเงินได้ 15% แต่ไม่เกิน 500,000 บาท
                                        </small>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="donation">เงินบริจาคทั่วไป</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="donation"
                                                   id="donation"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['donation'])
                                                   value="{{session()->get('allowances')['donation']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="donation" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 10% ของเงินได้
                                        </small>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="donations_for_education">เงินบริจาคเพื่อการศึกษา</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="donations_for_education"
                                                   id="donations_for_education"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['donations_for_education'])
                                                   value="{{session()->get('allowances')['donations_for_education']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="donations_for_education" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 10% ของเงินได้
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg w-25">ต่อไป</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/js/fill_zero.js"></script>

    <script>
        $('#donation,#donations_for_education').on('input', function () {
            var value = $(this).val();
            var net_income = {{session()->get('incomes')['net_income']}};
            var max = net_income * 0.1;
            if ((value !== '') && (value.indexOf('.') === -1)) {
                $(this).val(Math.max(Math.min(value, max), 0));
            }
        });
    </script>
@endpush
