@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="app" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li><a href="/user/tax/assessable_income">เงินได้</a></li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">รายการลดหย่อนส่วนบุคคล</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">ค่าใช้จ่าย</label>
                                <div class="input-group">
                                    <input id="expense"
                                           type="number"
                                           class="form-control" name="expense"
                                           aria-describedby="expense"
                                           placeholder=""
                                           @if(session()->has('expense'))
                                           value="{{session()->get('expense')}}"
                                           @else
                                           value="100000"
                                           @endif
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>

                                </div>
                                <small id="expense" class="form-text text-muted">ร้อยละ 50 แต่ไม่เกิน 100,000
                                    บาท
                                </small>
                            </div>
                            <div class="form-group">
                                <label for="">ลดหย่อนส่วนตัว</label>
                                <div class="input-group">
                                    <input id="personal"
                                           type="number"
                                           min="0"
                                           max="60000"
                                           class="form-control" name="personal"
                                           aria-describedby="personal" placeholder=""
                                           @isset(session()->get('allowances')['personal'])
                                           value="{{session()->get('allowances')['personal']}}"
                                           @endisset
                                           @empty(session()->get('allowances')['personal'])
                                           value="60000"
                                           @endempty
                                           required>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                                <small id="personal" class="form-text text-muted">คนละ 60,000 บาท</small>
                            </div>
                            @if(Auth::user()->Taxpayer->status=='สมรส(ยื่นร่วม)')
                                <div class="form-group">
                                    <label for="">ลดหย่อนคู่สมรส</label>
                                    <div class="input-group">
                                        <input id="spouse"
                                               type="number"
                                               min="0"
                                               max="60000"
                                               class="form-control" name="spouse"
                                               aria-describedby="spouse" placeholder=""
                                               @isset(session()->get('allowances')['spouse'])
                                               value="{{session()->get('allowances')['spouse']}}"
                                               @endisset
                                               @empty(session()->get('allowances')['spouse'])
                                               value="60000"
                                               @endempty
                                               required>
                                        <span class="input-group-append input-group-text">บาท / ปี</span>

                                    </div>
                                    <small id="spouse" class="form-text text-muted">คนละ 60,000 บาท</small>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary btn-lg w-25">ต่อไป</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('script')
    <script src="/js/fill_zero.js"></script>

@endpush