@extends('layouts.master')
@section('title','เงินได้พึงประเมิน')
@section('content')
    <div id="app" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li class="active">เงินได้</li>
                    <li>รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">เงินได้พึงประเมิน</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">เงินได้</h5>
                        <div class="form-group">
                            <label class="checkbox-inline">
                                <input type="radio" value="monthly"
                                >
                                เงินได้ต่อเดือน
                            </label>
                            <label class="checkbox-inline ml-4">
                                <input type="radio" value="yearly"
                                >
                                เงินได้ต่อปี
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <form action="" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="bonus">เงินเดือน</label>

                                        <div class="input-group">
                                            <input id="salary"
                                                   type="number"
                                                   class="form-control" name="salary"
                                                   aria-describedby="salary"
                                                   placeholder=""
                                                   @isset(session()->get('incomes')['salary'])
                                                   value="{{session()->get('incomes')['salary']}}"
                                                    @endisset>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="bonus">โบนัส</label>

                                        <div class="input-group">
                                            <input id="bonus"
                                                   type="number"
                                                   class="form-control" name="bonus"
                                                   aria-describedby="bonus"
                                                   placeholder=""
                                                   @isset(session()->get('incomes')['bonus'])
                                                   value="{{session()->get('incomes')['bonus']}}"
                                                    @endisset>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="dividend">เงินปันผล (บาท/เดือน)</label>
                                        <div class="input-group">
                                            <input id="dividend"
                                                   type="number"
                                                   class="form-control" name="dividend"
                                                   aria-describedby="dividend"
                                                   @isset(session()->get('incomes')['dividend'])
                                                   value="{{session()->get('incomes')['dividend']}}"
                                                    @endisset>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_income">รายได้ทั้งหมด (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input id="net_income"
                                                   type="number"
                                                   class="form-control" name="net_income"
                                                   aria-describedby="net_income"
                                                   placeholder=""
                                                   @isset(session()->get('incomes')['net_income'])
                                                   value="{{session()->get('incomes')['net_income']}}"
                                                   @endisset
                                                   readonly>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="holding_tax">ภาษีเงินได้ หัก ณ ที่จ่าย (บาท/ปี)</label>
                                        <div class="input-group">
                                            <input id="holding_tax"
                                                   type="number"
                                                   class="form-control" name="holding_tax"
                                                   aria-describedby="holding_tax"
                                                   placeholder=""
                                                   @isset(session()->get('incomes')['holding_tax'])
                                                   value="{{session()->get('incomes')['holding_tax']}}"
                                                    @endisset>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg w-25">บันทึกข้อมูล</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false"
         aria-hidden="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">กรุณาระบุสถานภาพผู้มีเงินได้</h5>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="0" value="0" checked>
                            <label class="form-check-label" for="0">
                                โสด
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="1" value="1" checked>
                            <label class="form-check-label" for="1">
                                สมรส (ยื่นร่วม)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="2" value="2" checked>
                            <label class="form-check-label" for="2">
                                สมรส (แยกยื่น)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="3" value="3" checked>
                            <label class="form-check-label" for="3">
                                หม้าย
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="status" id="4" value="4" checked>
                            <label class="form-check-label" for="4">
                                ตายระหว่างปี
                            </label>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    @if(!Auth::user()->Taxpayer)
        <script type="text/javascript">
            $(window).on('load', function () {
                $('#Modal1').modal('show');
            });
        </script>
    @endif
    <script src="/js/fill_zero.js"></script>
    <script>
        jQuery('document').ready(function () {
                jQuery('input').on('keyup', function () {

                    var salary = jQuery('#salary').val();
                    var bonus = jQuery('#bonus').val();
                    var dividend = jQuery('#dividend').val();
                    var net_income;

                    (salary === '') ? salary = 0 : '';
                    (bonus === '') ? bonus = 0 : '';
                    (dividend === '') ? dividend = 0 : '';

                    salary = parseInt(salary);
                    bonus = parseInt(bonus);
                    dividend = parseInt(dividend);

                    net_income = (salary * 12) + bonus + dividend;
                    jQuery('#net_income').val(net_income);
                });
            }
        );


    </script>

@endpush

