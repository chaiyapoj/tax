@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li><a href="/user/tax/assessable_income">เงินได้</a></li>
                    <li><a href="/user/tax/allowance1">รายการลดหย่อน</a></li>
                    <li class="active">รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการเงินที่ได้รับยกเว้น</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">รายการเงินที่ได้รับยกเว้น</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="provident_fund_contribution">เงินสะสมกองทุนสำรองเลี้ยงชีพ
                                        </label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="provident_fund_contribution"
                                                   id="provident_fund_contribution"
                                                   aria-describedby="provident_fund_contribution" placeholder=""
                                                   @isset(session()->get('exemptions')['provident_fund_contribution'])
                                                   value="{{session()->get('exemptions')['provident_fund_contribution']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="provident_fund_contribution" class="form-text text-muted">
                                            ส่วนที่เกิน 10,000 บาท
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label for="government_pension_fund_contribution">เงินสะสม กบข.</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="government_pension_fund_contribution"
                                                   id="government_pension_fund_contribution"
                                                   aria-describedby="government_pension_fund_contribution"
                                                   placeholder=""
                                                   @isset(session()->get('exemptions')['government_pension_fund_contribution'])
                                                   value="{{session()->get('exemptions')['government_pension_fund_contribution']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="private_teacher_aid_fund_contribution">เงินสะสมกองทุนสมเคราะห์ครูโรงเรียนเอกชน</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="private_teacher_aid_fund_contribution"
                                                   id="private_teacher_aid_fund_contribution"
                                                   aria-describedby="private_teacher_aid_fund_contribution"
                                                   placeholder=""
                                                   @isset(session()->get('exemptions')['private_teacher_aid_fund_contribution'])
                                                   value="{{session()->get('exemptions')['private_teacher_aid_fund_contribution']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="disabled_under_65">กรณีคนพิการที่มีอายุไม่เกิน 65 ปี</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="disabled_under_65"
                                                   id="disabled_under_65"
                                                   aria-describedby="disabled_under_65" placeholder=""
                                                   @isset(session()->get('exemptions')['disabled_under_65'])
                                                   value="{{session()->get('exemptions')['disabled_under_65']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="disabled_over_65">กรณีคนพิการที่มีอายุเกิน 65 ปี
                                            (รวมผู้พิการ)</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control" name="disabled_over_65"
                                                   id="disabled_over_65"
                                                   aria-describedby="disabled_over_65" placeholder=""
                                                   @isset(session()->get('exemptions')['disabled_over_65'])
                                                   value="{{session()->get('exemptions')['disabled_over_65']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="net_exemption">รวมเงินที่ได้รับยกเว้น</label>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control" name="net_exemption"
                                                   id="net_exemption"
                                                   aria-describedby="net_exemption" placeholder=""
                                                   @isset(session()->get('exemptions')['net_exemption'])
                                                   value="{{session()->get('exemptions')['net_exemption']}}"
                                                   @endisset
                                                   readonly>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="m-4">
                                <a href="/tax/step4">
                                    <button class="btn btn-secondary">ย้อนกลับ</button>
                                </a>
                                <button type="submit" class="btn btn-primary  btn-lg pull-right">ต่อไป</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>


@endsection

@push('script')
    <script src="/js/fill_zero.js"></script>
    <script>
        jQuery('document').ready(function () {
                jQuery('input').on('keyup', function () {

                    var provident_fund_contribution = jQuery('#provident_fund_contribution').val();
                    var government_pension_fund_contribution = jQuery('#government_pension_fund_contribution').val();
                    var private_teacher_aid_fund_contribution = jQuery('#private_teacher_aid_fund_contribution').val();
                    var disabled_under_65 = jQuery('#disabled_under_65').val();
                    var disabled_over_65 = jQuery('#disabled_over_65').val();
                    var net_exemption;

                    (provident_fund_contribution === '') ? provident_fund_contribution = 0 : '';
                    (government_pension_fund_contribution === '') ? government_pension_fund_contribution = 0 : '';
                    (private_teacher_aid_fund_contribution === '') ? private_teacher_aid_fund_contribution = 0 : '';
                    (disabled_under_65 === '') ? disabled_under_65 = 0 : '';
                    (disabled_over_65 === '') ? disabled_over_65 = 0 : '';

                    /*                    alert(provident_fund_contribution);
                                        alert(government_pension_fund_contribution);
                                        alert(private_teacher_aid_fund_contribution);
                                        alert(disabled_under_65);
                                        alert(disabled_over_65);*/

                    provident_fund_contribution = parseInt(provident_fund_contribution);
                    government_pension_fund_contribution = parseInt(government_pension_fund_contribution);
                    private_teacher_aid_fund_contribution = parseInt(private_teacher_aid_fund_contribution);
                    disabled_under_65 = parseInt(disabled_under_65);
                    disabled_over_65 = parseInt(disabled_over_65);

                    net_exemption = provident_fund_contribution + government_pension_fund_contribution + private_teacher_aid_fund_contribution + disabled_under_65 + disabled_over_65;
                    jQuery('#net_exemption').val(net_exemption);
                });
            }
        );


    </script>

@endpush

