@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li><a href="/user/tax/assessable_income">เงินได้</a></li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">ค่าลดหย่อนอื่นๆ</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="">เบี้ยประกันชีวิตของตนเอง</label>
                                        <div class="input-group">

                                            <input type="number"
                                                   min="0"
                                                   max="100000"
                                                   class="form-control" name="life_insurance_premium"
                                                   id="life_insurance_premium"
                                                   aria-describedby="life_insurance_premium" placeholder=""
                                                   @isset(session()->get('allowances')['life_insurance_premium'])
                                                   value="{{session()->get('allowances')['life_insurance_premium']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="life_insurance_premium" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 100,000 บาท
                                        </small>
                                    </div>
                                    <div class="form-group">
                                        <label for="">เบี้ยประกันชีวิตแบบบำนาญของตนเอง</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="200000"
                                                   class="form-control" name="pension_insurance_premium"
                                                   id="pension_insurance_premium"
                                                   aria-describedby="pension_insurance_premium" placeholder=""
                                                   @isset(session()->get('allowances')['pension_insurance_premium'])
                                                   value="{{session()->get('allowances')['pension_insurance_premium']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <small id="pension_insurance_premium" class="form-text text-muted">คิดจากเงินได้
                                        15%
                                        แต่ไม่เกิน 200,000 บาท
                                    </small>
                                    <div class="form-group">
                                        <label for="parental_health_insurance_premium">เบี้ยประกันสุขภาพ บิดา-มารดา
                                            ของตนเอง</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="30000"
                                                   class="form-control" name="parental_health_insurance_premium"
                                                   id="parental_health_insurance_premium"
                                                   aria-describedby="parental_health_insurance_premium" placeholder=""
                                                   @isset(session()->get('allowances')['parental_health_insurance_premium'])
                                                   value="{{session()->get('allowances')['parental_health_insurance_premium']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="parental_health_insurance_premium" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 15,000 บาท ต่อคน
                                        </small>
                                    </div>
                                </div>
                                    <div class="col-6">
                                        @if(Auth::user()->Taxpayer->status=='สมรส(ยื่นร่วม)')

                                        <div class="form-group">
                                            <label for="spouse_life_insurance_premium">เบี้ยประกันชีวิตของคู่สมรส</label>
                                            <div class="input-group">
                                                <input type="number"
                                                       min="0"
                                                       max="10000"
                                                       class="form-control" name="spouse_life_insurance_premium"
                                                       id="spouse_life_insurance_premium"
                                                       aria-describedby="spouse_life_insurance_premium" placeholder=""
                                                       @isset(session()->get('allowances')['spouse_life_insurance_premium'])
                                                       value="{{session()->get('allowances')['spouse_life_insurance_premium']}}"
                                                       @endisset
                                                       required>
                                                <span class="input-group-append input-group-text">บาท / ปี</span>
                                            </div>
                                            <small id="spouse_life_insurance_premium" class="form-text text-muted">
                                                เท่าที่จ่ายจริง แต่ไม่เกิน 10,000 บาท
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="spouse_pension_insurance_premium">เบี้ยประกันชีวิตแบบบำนาญของคู่สมรส</label>
                                            <div class="input-group">
                                                <input type="number"
                                                       min="0"
                                                       max="10000"
                                                       class="form-control" name="spouse_pension_insurance_premium"
                                                       id="spouse_pension_insurance_premium"
                                                       aria-describedby="spouse_pension_insurance_premium"
                                                       placeholder=""
                                                       @isset(session()->get('allowances')['spouse_pension_insurance_premium'])
                                                       value="{{session()->get('allowances')['spouse_pension_insurance_premium']}}"
                                                       @endisset
                                                       required>
                                                <span class="input-group-append input-group-text">บาท / ปี</span>
                                            </div>
                                        </div>
                                        <small id="spouse_pension_insurance_premium" class="form-text text-muted">
                                            เท่าที่จ่ายจริง
                                            แต่ไม่เกิน 10,000 บาท
                                        </small>
                                    </div>
                                @endif

                            </div>
                            <button type="submit" class="btn btn-primary btn-lg w-25">ต่อไป</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/js/fill_zero.js"></script>
@endpush