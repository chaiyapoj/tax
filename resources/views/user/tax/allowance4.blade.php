@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li><a href="/user/tax/assessable_income">เงินได้</a></li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">ค่าลดหย่อนอื่นๆ</h5>
                        <form action="" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    @if(Auth::user()->Taxpayer->status=='สมรส(ยื่นร่วม)')
                                        <div class="form-group">
                                            <label for="spouse_parental_health_insurance_premium">เบี้ยประกันสุขภาพบิดา-มารดาของคู่สมรส</label>
                                            <div class="input-group">

                                                <input type="number"
                                                       min="0"
                                                       max="30000"
                                                       class="form-control"
                                                       name="spouse_parental_health_insurance_premium"
                                                       id="spouse_parental_health_insurance_premium"
                                                       placeholder=""
                                                       @isset(session()->get('allowances')['spouse_parental_health_insurance_premium'])
                                                       value="{{session()->get('allowances')['spouse_parental_health_insurance_premium']}}"
                                                       @endisset
                                                       required>
                                                <span class="input-group-append input-group-text">บาท / ปี</span>
                                            </div>
                                            <small id="spouse_parental_health_insurance_premium"
                                                   class="form-text text-muted">
                                                เท่าที่จ่ายจริง แต่ไม่เกิน 15,000 บาท ต่อคน
                                            </small>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="home_mortgage_interest">ดอกเบี้ยเงินกู้ยืมเพื่อการมีที่อยู่อาศัย</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="100000"
                                                   class="form-control" name="home_mortgage_interest"
                                                   id="home_mortgage_interest"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['home_mortgage_interest'])
                                                   value="{{session()->get('allowances')['home_mortgage_interest']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                    </div>
                                    <small id="home_mortgage_interest" class="form-text text-muted">เท่าที่จ่ายจริง
                                        แต่ไม่เกิน 100,000 บาท
                                    </small>
                                    <div class="form-group">
                                        <label for="provident_fund_contribution">กองทุนสำรองเลี้ยงชีพ</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="10000"
                                                   class="form-control" name="provident_fund_contribution"
                                                   id="provident_fund_contribution"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['provident_fund_contribution'])
                                                   value="{{session()->get('allowances')['provident_fund_contribution']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="provident_fund_contribution" class="form-text text-muted">
                                            ไม่เกิน 10,000 บาท ส่วนที่เกินจะได้รับเป็นเงินที่ได้รับยกเว้น
                                        </small>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="social_security_fund_contribution">กองทุนประกันสังคมของตนเอง</label>
                                        <div class="input-group">
                                            <input type="number"
                                                   min="0"
                                                   max="9000"
                                                   class="form-control" name="social_security_fund_contribution"
                                                   id="social_security_fund_contribution"
                                                   placeholder=""
                                                   @isset(session()->get('allowances')['social_security_fund_contribution'])
                                                   value="{{session()->get('allowances')['social_security_fund_contribution']}}"
                                                   @endisset
                                                   required>
                                            <span class="input-group-append input-group-text">บาท / ปี</span>
                                        </div>
                                        <small id="social_security_fund_contribution" class="form-text text-muted">
                                            เท่าที่จ่ายจริง แต่ไม่เกิน 9,000 บาท
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg w-25">ต่อไป</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/js/fill_zero.js"></script>


@endpush