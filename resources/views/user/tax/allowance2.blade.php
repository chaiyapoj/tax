@extends('layouts.master')
@section('title','รายการลดหย่อนและหักค่าใช้จ่าย')
@section('content')
    <div id="app" class="container">
        <div class="row p-4">
            <div class="col-12">
                <ul class="progressbar">
                    <li><a href="/user/tax/assessable_income">เงินได้</a></li>
                    <li class="active">รายการลดหย่อน</li>
                    <li>รายการยกเว้น</li>
                    <li>สรุป</li>
                </ul>
            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">หน้าหลัก</li>
            <li class="breadcrumb-item">คำนวนภาษี</li>
            <li class="breadcrumb-item active" aria-current="page">รายการลดหย่อนและหักค่าใช้จ่าย</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body ml-4">
                        <h5 class="card-title">รายการลดหย่อนส่วนบุคคล</h5>
                        <form action="" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="parental_care">อุปการะเลี้ยงดูบิดา-มารดาตนเอง</label>
                                <div class="input-group">
                                    <input min="0" max="2" name="parental_count" id="parental_count"
                                           @isset(session()->get('allowances')['parental_count'])
                                           value="{{session()->get('allowances')['parental_count']}}"
                                           @endisset
                                           type="number">
                                    <span class="input-group-append input-group-text mr-4">คน</span>
                                    <input type="number"
                                           min="0"
                                           max="60000"
                                           class="form-control" name="parental_care"
                                           id="parental_care"
                                           aria-describedby="parental_care" placeholder=""
                                           @isset(session()->get('allowances')['parental_care'])
                                           value="{{session()->get('allowances')['parental_care']}}"
                                           @endisset
                                           required readonly>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                                <small class="form-text text-muted">คนละ 30,000 บาท สูงสุด 2
                                    คน
                                </small>
                            </div>
                            @if(Auth::user()->Taxpayer->status=='สมรส(ยื่นร่วม)')

                                <div class="form-group">
                                    <label for="spouse_parental_care">อุปการะเลี้ยงดูบิดา-มารดาของคู่สมรส</label>
                                    <div class="input-group">
                                        <input min="0" max="2" name="spouse_parental_count" id="spouse_parental_count"
                                               @isset(session()->get('allowances')['spouse_parental_count'])
                                               value="{{session()->get('allowances')['spouse_parental_count']}}"
                                               @endisset
                                               type="number">
                                        <span class="input-group-append input-group-text mr-4">คน</span>
                                        <input type="number"
                                               min="0"
                                               max="60000"
                                               class="form-control" name="spouse_parental_care"
                                               id="spouse_parental_care"
                                               aria-describedby="spouse_parental_care" placeholder=""
                                               @isset(session()->get('allowances')['spouse_parental_care'])
                                               value="{{session()->get('allowances')['spouse_parental_care']}}"
                                               @endisset
                                               required readonly>
                                        <span class="input-group-append input-group-text">บาท / ปี</span>
                                    </div>
                                </div>
                            @endif
                            <small id="spouse_parental_care" class="form-text text-muted">คนละ 30,000 บาท
                                สูงสุด 2 คน
                            </small>
                            <div class="form-group">
                                <label for="disabled_care">อุปการะเลี้ยงดูคนพิการหรือคนทุพพลภาพ</label>
                                <div class="input-group">
                                    <input min="0" max="1" name="disabled_care_count" id="disabled_care_count"
                                           @isset(session()->get('allowances')['disabled_care_count'])
                                           value="{{session()->get('allowances')['disabled_care_count']}}"
                                           @endisset
                                           type="number">
                                    <span class="input-group-append input-group-text mr-4">คน</span>
                                    <input type="number"
                                           min="0"
                                           max="30000"
                                           class="form-control" name="disabled_care"
                                           id="disabled_care"
                                           aria-describedby="disabled_care" placeholder=""
                                           @isset(session()->get('allowances')['disabled_care'])
                                           value="{{session()->get('allowances')['disabled_care']}}"
                                           @endisset
                                           required readonly>
                                    <span class="input-group-append input-group-text">บาท / ปี</span>
                                </div>
                                <small id="disabled_care" class="form-text text-muted">สูงสุุด 1 คน 30,000
                                    บาท
                                </small>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg w-25">ต่อไป</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="/js/fill_zero.js"></script>
    <script>
        jQuery('document').ready(function () {
                $("#parental_count").change(function () {
                    var total = parseFloat($("#parental_count").val()) * 30000;
                    $('#parental_care').val(total);
                });

                $("#spouse_parental_count").change(function () {
                    var total = parseFloat($("#spouse_parental_count").val()) * 30000;
                    $('#spouse_parental_care').val(total);
                });

                $("#disabled_care_count").change(function () {
                    var total = parseFloat($("#disabled_care_count").val()) * 30000;
                    $('#disabled_care').val(total);
                });
            }
        );
    </script>
@endpush

