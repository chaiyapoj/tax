@extends('layouts.master')
@section('title','สถานะภาพของผู้มีเงินได้')


@section('content')


    <div class="container-fluid">
        <div class="">
            <div class="card mt-5">
                <div class="card-header">
                    Welcome to Easy Tax2
                </div>
                <div class="card-body mx-auto text-center col">
                    <img src="/webp/logo-index.webp">
                    <p class="card-text text-center">กรุณา Login ก่อน</p>
                    <div class="row">
                        @guest('web')
                            <div class="mx-auto">
                                <a type="button" class="mx-2  btn btn-warning btn-lg">
                                    <i class="fa fa-calculator" aria-hidden="true"></i> คำนวณภาษี</a>
                                <a type="button" class="btn btn-warning btn-lg">
                                    <i class="fa fa-user-plus" aria-hidden="true"></i> ลงทะเบียน
                                </a>
                                <a type="button" class="mx-2 btn btn-warning btn-lg">
                                    <i class="fa fa-user" aria-hidden="true"></i> เข้าสู่ระบบ
                                </a>

                            </div>
                        @endguest
                        @auth('web')
                            <div class=""><a href="/user/tax">
                                    <button type="button" class="mx-2  btn btn-warning btn-lg">
                                        <i class="fa fa-calculator" aria-hidden="true"></i> คำนวณภาษี
                                    </button>
                                </a>
                            </div>
                            <div class="col-2"><a href="/logout">
                                    <button type="button" class="btn btn-warning btn-lg">
                                        <i class="fa fa-remove" aria-hidden="true"></i> ออกจากระบบ
                                    </button>
                                </a></div>
                        @endauth
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection
