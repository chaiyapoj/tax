<!doctype html>
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/css/app.css">
    @stack('css')
    <title>EasyTax @yield('title')</title>
</head>
<body>

<div class="app">
    <div class="app-body">
        <div class="app-sidebar sidebar-dark sidebar-slide-left">
            <div class="text-right">
                <button type="button" class="btn btn-sidebar" data-dismiss="sidebar">
                    <span class="x"></span>
                </button>
            </div>
            @include('layouts.partials.sidebar')
        </div>

        <div class="app-content" id="app">
            <nav class="navbar navbar-expand navbar-light bg-white">
                <button type="button" class="btn btn-sidebar" data-toggle="sidebar">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="navbar-brand">
                    EasyTax
                </div>
                @guest('web')
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="/login" class="dropdown-item">
                                    <div>เข้าสู่ระบบ</div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="/register" class="dropdown-item">
                                    <div>สมัครสมาชิก</div>

                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="/password-reset" class="dropdown-item">
                                    <div>ลืมรหัสผ่าน</div>
                                </a>
                            </div>
                        </li>
                    </ul>

                @endguest

                @auth('web')
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <span>เมนูสมาชิก</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="/change-password" class="dropdown-item">
                                    <div>เปลี่ยนรหัสผ่าน</div>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="/logout" class="dropdown-item">
                                    <div>ออกจากระบบ</div>
                                </a>
                            </div>
                        </li>
                    </ul>
                @endauth

            </nav>
            @yield('content')
        </div>
    </div>
</div>


<script src="/js/app.js"></script>
<script src="/js/init.js"></script>
<script src="/js/admin4b.min.js"></script>
<script>

</script>
@stack('js')

@stack('script')
@LaravelSweetAlertJS


</body>
</html>
