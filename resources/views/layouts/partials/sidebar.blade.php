<div class="sidebar-header">
    <img src="/webp/logo.webp">
</div>
<div id="sidebar-nav" class="sidebar-nav" data-children=".sidebar-nav-group">
    <a href="/" class="sidebar-nav-link {{ Request::is('/') ? 'active' : '' }}"><i class="fa fa-line-chart"
                                                                                   aria-hidden="true"></i>หน้าหลัก</a>
    @guest('web')
    <a href="/tax" class="sidebar-nav-link {{ Request::is('/tax/*') ? 'active' : '' }}"><i class="fa fa-wrench"
    aria-hidden="true"></i>คำนวนภาษี
    </a>
    @endguest
    @auth('web')
        <div class="sidebar-nav-group">
            <a href="#profile" class="sidebar-nav-link collapsed  {{ Request::is('/user/profile/*') ? 'active' : '' }}"
               data-toggle="collapse" data-parent="#sidebar-nav"><i
                        class="fa fa-user"></i>ข้อมูลส่วนตัว</a>
            <div id="profile" class="sidebar-nav-group collapse {{ Request::is('user/profile*') ? 'show' : '' }}">
                <a href="/user/profile/taxpayer"
                   class="sidebar-nav-link {{ Request::is('user/profile/taxpayer') ? 'active' : '' }}">ข้อมูลผู้มีเงินได้</a>
                @if(Auth::user()->Taxpayer()->exists())
                    @if(Auth::user()->Taxpayer->status=='สมรส(ยื่นร่วม)'||Auth::user()->Taxpayer->status=='สมรส(แยกยื่น)')
                        <a href="/user/profile/spouse"
                           class="sidebar-nav-link {{ Request::is('/user/profile*') ? 'active' : '' }}">
                            ข้อมูลคู่สมรส
                        </a>
                    @endif
                <a href="/user/profile/parental"
                   class="sidebar-nav-link {{ Request::is('/user/profile*') ? 'active' : '' }}">
                    ข้อมูลบิดา-มารดา
                </a>
                <a href="/user/profile/child"
                   class="sidebar-nav-link {{ Request::is('/user/profile*') ? 'active' : '' }}">
                    ข้อมูลบุตร
                </a>
                <a href="/user/profile/disabled"
                   class="sidebar-nav-link {{ Request::is('/user/profile*') ? 'active' : '' }}">
                    ข้อมูลผู้พิการหรือทุพพลภาพ
                </a>
                @endif

            </div>
        </div>
        <div class="sidebar-nav-group">
            <a href="#calculate"
               class="sidebar-nav-link collapsed "
               data-toggle="collapse" data-parent="#sidebar-nav"><i class="fa fa-calculator" aria-hidden="true"></i>การคำนวณภาษี
            </a>
            <div id="calculate" class="sidebar-nav-group collapse {{ Request::is('user/tax*') ? 'show' : '' }}">
                <a href="/user/tax/assessable_income" class="sidebar-nav-link {{ Request::is('/user/tax/assessable_income') ? 'active' : '' }}">
                    เงินได้พึงประเมิน
                </a>
                <a href="/user/tax/allowance1"
                   class="sidebar-nav-link {{ Request::is('/user/tax/allowance1') ? 'active' : '' }}">
                    ค่าลดหย่อนส่วนบุคคล
                </a>
                <a href="/user/tax/allowance2"
                   class="sidebar-nav-link {{ Request::is('/user/tax/allowance2') ? 'active' : '' }}">
                    ค่าลดหย่อนอื่นๆ
                </a>
                <a href="/user/tax/exemption"
                   class="sidebar-nav-link {{ Request::is('/tax/exemption') ? 'active' : '' }}">
                    รายการเงินได้ที่ได้รับการยกเว้น
                </a>
            </div>
        </div>
        <a href="/user/tax/calculate" class="sidebar-nav-link {{ Request::is('/tax/*') ? 'active' : '' }}"><i
                    class="fa fa-line-chart"
                    aria-hidden="true"></i>สรุปค่าภาษี</a>
{{--        <a href="page.html" class="sidebar-nav-link {{ Request::is('/tax/*') ? 'active' : '' }}"><i class="fa fa-wrench"
                                                                                                    aria-hidden="true"></i>แก้ไขข้อมูล
        </a>--}}
        <a href="/export" class="sidebar-nav-link {{ Request::is('/tax/*') ? 'active' : '' }}"><i class="fa fa-print"
                                                                                                  aria-hidden="true"></i>พิมพ์แบบแสดงรายการ
        </a>
    @endauth
</div>

<div class="sidebar-footer">
    <span class="text-light small">Developed by RMUTNB Student.</span>
</div>