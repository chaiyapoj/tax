let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    // .copy('resources/vendor/admin4b/dist/init.js', 'public/js')
    .copy('resources/vendor/admin4b/dist/admin4b.min.js', 'public/js/admin4b.min.js')
    .copy('resources/assets/css/my-login.css', 'public/css/my-login.css')
    .copy('resources/assets/js/my-login.js', 'public/js/my-login.js');
// mix.combine('resources/assets/js/admin4b/*', 'public/js/admin4b.js');