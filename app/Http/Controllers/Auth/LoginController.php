<?php

    namespace App\Http\Controllers\Auth;

    use App\Data;
    use App\Http\Controllers\Controller;
    use App\Profile;
    use App\Taxpayer;
    use App\User;
    use Illuminate\Foundation\Auth\AuthenticatesUsers;
    use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Facades\Auth;

    class LoginController extends Controller
    {
        /*
        |--------------------------------------------------------------------------
        | Login Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles authenticating users for the application and
        | redirecting them to your home screen. The controller uses a trait
        | to conveniently provide its functionality to your applications.
        |
        */

        use AuthenticatesUsers;

        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo = '/';

        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
            $this->middleware('guest')->except('logout');
        }

        /**
         * The user has been authenticated.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  mixed $user
         * @return mixed
         */
        protected function authenticated()
        {
            $user = Auth::User();
            Session()->put('incomes', $user->incomes);
            Session()->put('expense', $user->expense);
            Session()->put('allowances', $user->allowances);
            Session()->put('exemptions', $user->exemptions);

            return 0;

        }


        protected function logout()
        {
            Session::flush();

            LaravelSweetAlert::setMessageSuccess("flash message");
            return redirect('/');
        }
    }


