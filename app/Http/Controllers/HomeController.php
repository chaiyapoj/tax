<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Routing\Route;
    use Illuminate\Support\Facades\Auth;
    use App\User;

    class HomeController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
        /*    public function __construct()
            {
                $this->middleware('auth');
            }*/

        public function index()
        {
            return view('index');
        }

        public function taxCalculator()
        {
            return view('tax.index');
        }

        public function bypass()
        {
            if (!Auth::check()) {
                $user = User::find('1');
                Auth::login($user);
                return redirect()->back();
            } else {
                Auth::logout();
                return redirect()->back();
            }
        }
    }
