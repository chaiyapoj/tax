<?php

namespace App\Http\Controllers;

use mikehaertl\pdftk\Pdf;
use mikehaertl\pdftk\XfdfFile;


class TestController extends Controller
{
    public function index()
    {

        $xfdf = new XfdfFile(['text16' => iconv('UTF-8', 'TIS-620', 'ดกหด')]);
        $xfdf->saveAs(public_path('save/output2.xfdf'));


        $pdf = new Pdf(public_path('filled.pdf'));
        $pdf->fillForm(public_path('save/output2.xfdf'))
            ->needAppearances()->saveAs(public_path('save/output2.pdf'));
    }
}
