<?php

    namespace App\Http\Controllers;

    use App\Data;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Session;
    use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;

    class TaxController extends Controller
    {


        public function step1(Request $request)
        {
            $input_data = $request->except('_token');

            if (Auth::check()) {

                if (!$request->session()->has('data')) {
                    $data = SomeModel::find(1)->sessions;
                    Session::put('data', $data);
                }


            }


            Session::put('data', $input_data);

            if (Auth::check()) {
                $data = Auth::user()->data;
                $data->sessions = $input_data;
                $data->save();
            }
            return redirect('tax/step2');


        }



        public function step2(Request $request)
        {

            Session::put('data', $request->except('_token'));

            return redirect('tax/step3');
        }

        public function step3(Request $request)
        {

            Session::put('data', $request->except('_token'));


            return redirect('tax/step4');
        }

        public function step4(Request $request)
        {
            Session::put('data', $request->except('_token'));

            return redirect('tax/step5');
        }

        public function step5(Request $request)
        {
            Session::put('data', $request->except('_token'));
            return redirect('tax/step6');

        }

        public function storeData()
        {
            Data::firstOrNew(Session::get('data'));
            LaravelSweetAlert::setMessageSuccess("บันทึกข้อมูลเรียบร้อย!");
        }
    }
