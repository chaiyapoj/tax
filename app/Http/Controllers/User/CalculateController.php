<?php

    namespace App\Http\Controllers\User;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Facades\Auth;
    use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;
//    use madnh/fpdm;

    class CalculateController extends Controller
    {
        public function Calculate()
        {
            $net_tax = 0;

            //ข้อมูลจริงจากผู้ใช้
            $expense = Session::get('expense');
            $allowances = Session()->get('allowances');
            $incomes = Session()->get('incomes');
            $exemptions = Session()->get('exemptions');
            if (is_null($expense) || is_null($allowances) || is_null($incomes) || is_null($exemptions)) {
                LaravelSweetAlert::setMessageError('กรุณากรอกข้อมูลเ การคำนวณภาษีก่อน');
                return redirect()->back();
//                return redirect('/user/tax');
            }

            if ($expense / 2 <= 100000) {
                $deductible_expense = $expense * 0.5;
            } else {
                $deductible_expense = 100000;
            }

            $gross_income = array_sum($incomes);
            $gross_allowance = array_sum($allowances);
            $gross_exemption = array_sum($exemptions);

            $net_income = $gross_income - $deductible_expense - $gross_allowance - $gross_exemption;

            switch ($net_income) {
                case $net_income <= 150000:
                    $net_tax = 0;
                    break;

                case $net_income <= 300000:
                    $net_tax = ($net_income - 150000) * 0.05;
                    break;

                case $net_income <= 500000:
                    $net_tax = (($net_income - 300000) * 0.1) + 7500;
                    break;

                case $net_income <= 750000:
                    $net_tax = (($net_income - 500000) * 0.15) + 27500;
                    break;

                case $net_income <= 1000000:
                    $net_tax = (($net_income - 750000) * 0.2) + 65000;
                    break;

                case $net_income <= 2000000:
                    $net_tax = (($net_income - 1000000) * 0.25) + 115000;
                    break;

                case $net_income <= 5000000:
                    $net_tax = (($net_income - 2000000) * 0.3) + 365000;
                    break;

                case $net_income > 5000000:
                    $net_tax = (($net_income - 5000000) * 0.35) + 1265000;
                    break;
            }
            return $net_tax;

        }


    }