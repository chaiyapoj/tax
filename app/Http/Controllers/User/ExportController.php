<?php

    namespace App\Http\Controllers\User;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use fpdm\FPDM;
    use App\Taxpayer;
    use Illuminate\Support\Facades\Auth;
    use Carbon\Carbon;

    class ExportController extends Controller
    {
        public function exportPDF()
        {

            $taxpayer = Auth::User()->Taxpayer;

            $fields['id_card'] = $taxpayer->id_card;
            $fields['first_name'] = $taxpayer->first_name;
 /*           $fields['dob'] = Carbon::parse($taxpayer->birth_date)->format('d');
            $fields['mob'] = Carbon::parse($taxpayer->birth_date)->format('m');
            $fields['yob'] = Carbon::parse($taxpayer->birth_date)->format('y');*/
            $fields['building'] = $taxpayer->building;
            $fields['village'] = $taxpayer->village;
            $fields['house_no'] = $taxpayer->house_no;
            $fields['village_no'] = $taxpayer->village_no;
            $fields['lane'] = $taxpayer->lane;
            $fields['road'] = $taxpayer->road;
            $fields['district'] = $taxpayer->district;
//            $fields['province'] = $taxpayer->province;
            $fields['postal_code'] = 'มานี';
            $fields['last_name'] = $taxpayer->last_name;

            $pdf = new FPDM('tax.pdf');
            $pdf->Load($fields, false); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
            $pdf->Merge();
            $pdf->Output();
        }


    }