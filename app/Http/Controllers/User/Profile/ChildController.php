<?php

    namespace App\Http\Controllers\User\Profile;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Child;
    use Auth;

    class ChildController extends Controller
    {
        public function index()
        {
            $children = Child::all();
            return view('user.profile.child.index')->with(compact('children'));
        }

        public function create()
        {
            return view('user.profile.child.create');

        }

        public function store(Request $request)
        {

            $child = new Child();
            $child->taxpayer_id = Auth::id();
            $child->pre_name = $request->get('pre_name');
            $child->first_name = $request->get('first_name');
            $child->last_name = $request->get('last_name');
            $child->birth_date = $request->get('birth_date');
            $child->type = $request->get('type');
            $child->save();

            return redirect('user/profile/child');

        }

        public function edit($id)
        {
            $child = Child::find($id);
            return view('user.profile.child.edit')->with(compact('child'));
        }

        public function update($id, Request $request)
        {
            $child = Child::find($id);
            $child->pre_name = $request->get('pre_name');
            $child->first_name = $request->get('first_name');
            $child->last_name = $request->get('last_name');
            $child->birth_date = $request->get('birth_date');
            $child->type = $request->get('type');
            $child->save();

            return redirect('user/profile/child');
        }


        public function delete($id)
        {
            Child::destroy($id);
            return redirect('user/profile/child');

        }
    }
