<?php

    namespace App\Http\Controllers\User\Profile;

    use App\Disabled;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Auth;

    class DisabledController extends Controller
    {
        public function index()
        {
            $disableds = Disabled::all();
            return view('user.profile.disabled.index')->with(compact('disableds'));
        }

        public function create()
        {
            return view('user.profile.disabled.create');

        }

        public function store(Request $request)
        {

            $disabled = new Disabled();
            $disabled->taxpayer_id = Auth::id();
            $disabled->pre_name = $request->get('pre_name');
            $disabled->first_name = $request->get('first_name');
            $disabled->last_name = $request->get('last_name');
            $disabled->birth_date = $request->get('birth_date');
            $disabled->type = $request->get('type');
            $disabled->save();

            return redirect('user/profile/disabled');

        }

        public function edit($id)
        {
            $disabled = Disabled::find($id);
            return view('user.profile.disabled.edit')->with(compact('disabled'));
        }

        public function update($id, Request $request)
        {
            $disabled = Disabled::find($id);
            $disabled->pre_name = $request->get('pre_name');
            $disabled->first_name = $request->get('first_name');
            $disabled->last_name = $request->get('last_name');
            $disabled->birth_date = $request->get('birth_date');
            $disabled->type = $request->get('type');
            $disabled->save();

            return redirect('user/profile/disabled');
        }


        public function delete($id)
        {
            Disabled::destroy($id);
            return redirect('user/profile/disabled');

        }
    }
