<?php

    namespace App\Http\Controllers\User;

    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Input;
    use App\Taxpayer;

    class TaxController extends Controller
    {
        public function AssessableIncome()
        {

            return view('user.tax.assessable_income');
        }

        public function StoreAssessableIncome(Request $request)
        {

            if ($request->has('status')) {
                $taxpayer = Auth::user()->Taxpayer;
                if (empty($taxpayer)) {
                    $taxpayer = new Taxpayer();
                    $taxpayer->user_id = Auth::id();
                } else {
                    $taxpayer = Auth::user()->Taxpayer;
                }
                $taxpayer->status = $request->get('status');
                $taxpayer->save();

                return redirect()->back();
            }


            $input = $request->except('_token');
            foreach ($input as $key => $value) {
                Session::put('incomes.' . $key, $value);
            }
            $incomes = session::get('incomes');
            $user = Auth::user();
            $user->incomes = $incomes;
            $user->save();

            return redirect('/user/tax/allowance1');
        }

        public function allowance1()
        {
            return view('user.tax.allowance1');
        }

        public function StoreAllowance1(Request $request)
        {
            $input_expense = $request->get('expense');
            Session::put('expense', $input_expense);

            $input_allowances = $request->except('_token', 'expense');
            foreach ($input_allowances as $key => $value) {
                Session::put('allowances.' . $key, $value);
            }

            $expense = session()->get('expense');;
            $allowances = session()->get('allowances');;

            $user = Auth::user();
            $user->expense = $expense;
            $user->allowances = $allowances;
            $user->save();

            return redirect('/user/tax/allowance2');
        }

        public function allowance2()
        {
            return view('user.tax.allowance2');

        }

        public function StoreAllowance2(Request $request)
        {
            $input = $request->except('_token');
            foreach ($input as $key => $value) {
                Session::put('allowances.' . $key, $value);
            }

            $allowances = session()->get('allowances');;
            $user = Auth::user();
            $user->allowances = $allowances;
            $user->save();

            return redirect('/user/tax/allowance3');
        }

        public function allowance3()
        {
            return view('user.tax.allowance3');
        }

        public function StoreAllowance3(Request $request)
        {
            $input = $request->except('_token');
            foreach ($input as $key => $value) {
                Session::put('allowances.' . $key, $value);
            }

            $allowances = session()->get('allowances');;
            $user = Auth::user();
            $user->allowances = $allowances;
            $user->save();

            return redirect('/user/tax/allowance4');
        }

        public function allowance4()
        {
            return view('user.tax.allowance4');

        }

        public function StoreAllowance4(Request $request)
        {
            $input = $request->except('_token');
            foreach ($input as $key => $value) {
                Session::put('allowances.' . $key, $value);
            }

            $allowances = session()->get('allowances');;
            $user = Auth::user();
            $user->allowances = $allowances;
            $user->save();

            return redirect('/user/tax/allowance5');


        }

        public function allowance5()
        {
            return view('user.tax.allowance5');

        }

        public function StoreAllowance5(Request $request)
        {
            $input = $request->except('_token');
            foreach ($input as $key => $value) {
                Session::put('allowances.' . $key, $value);
            }

            $allowances = session()->get('allowances');;
            $user = Auth::user();
            $user->allowances = $allowances;
            $user->save();

            return redirect('/user/tax/exemption');
        }

        public function exemption()
        {
            return view('user.tax.exemption');

        }

        public function StoreExemption(Request $request)
        {
            $input = $request->except('_token');
            foreach ($input as $key => $value) {
                Session::put('exemptions.' . $key, $value);
            }

            $exemptions = session()->get('exemptions');;
            $user = Auth::user();
            $user->exemptions = json_encode($exemptions);
            $user->save();

            return redirect('/user/tax/summary');

        }


    }
