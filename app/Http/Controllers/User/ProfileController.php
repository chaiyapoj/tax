<?php

namespace App\Http\Controllers\User;

use App\Disabled;
use App\Http\Controllers\Controller;
use App\Parental;
use App\Spouse;
use App\Taxpayer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;

class ProfileController extends Controller
{
    public function taxpayer()
    {
        $taxpayer = Auth::user()->Taxpayer;
        if (empty($taxpayer)) {
            return view('user.profile.taxpayer');
        } else {
            return view('user.profile.taxpayer')->with(compact('taxpayer'));

        }

    }

    public function storeTaxpayer(Request $request)
    {

        $taxpayer = Auth::user()->Taxpayer;
        if (empty($taxpayer)) {
            $taxpayer = new Taxpayer();
            $taxpayer->user_id = Auth::id();
        }
        $taxpayer->pre_name = $request->get('pre_name');
        $taxpayer->first_name = $request->get('first_name');
        $taxpayer->last_name = $request->get('last_name');
        $taxpayer->birth_date = $request->get('birth_date');
        $taxpayer->id_card = $request->get('id_card');
        $taxpayer->building = $request->get('building');
        $taxpayer->room = $request->get('room');
        $taxpayer->floor = $request->get('floor');
        $taxpayer->village = $request->get('village');
        $taxpayer->house_no = $request->get('house_no');
        $taxpayer->village_no = $request->get('village_no');
        $taxpayer->lane = $request->get('lane');
        $taxpayer->road = $request->get('road');
        $taxpayer->sub_district = $request->get('sub_district');
        $taxpayer->district = $request->get('district');
        $taxpayer->province = $request->get('province');
        $taxpayer->postal_code = $request->get('postal_code');
        $taxpayer->status = $request->get('status');
        $taxpayer->save();

        if ($taxpayer->status == 'สมรส(ยื่นร่วม)' || $taxpayer->status == 'สมรส(แยกยื่น)' && empty(Auth()->spouse)) {
            LaravelSweetAlert::setMessageSuccess('กรุณากรอกข้อมูลคู่สมรส');

            return redirect('user/profile/spouse');
        } else {
            LaravelSweetAlert::setMessageSuccess('บันทึกข้อมูลสำเร็จ');
            return redirect()->back();
        }
    }


    public function Spouse()
    {
        $taxpayer = Auth::user()->Taxpayer;
        if (isset($taxpayer)) {
            if ($taxpayer->status == 'สมรส(ยื่นร่วม)' || $taxpayer->status == 'สมรส(แยกยื่น)') {
                if (empty($taxpayer->spouse)) {
                    return view('user.profile.spouse');
                } else {
                    return view('user.profile.spouse')->with(compact($taxpayer->spouse));
                }
            } else {
                LaravelSweetAlert::setMessageError('สถานะของท่านต้องเป็น สมรส จึงจะสามารถกรอกข้อมูลคู่สมรสได้');
                return redirect()->back();
            }
        } else {
            LaravelSweetAlert::setMessageError('กรุณากรอกข้อมูลผู้มีเงินได้');

            return redirect('user/profile/taxpayer');
        }
    }


    public function storeSpouse(Request $request)
    {
        $taxpayer = Auth::user()->Taxpayer;

        $input = $request->except('_token');
        $input['taxpayer_id'] = $taxpayer->id;

//        dump($input);

        Spouse::updateOrCreate($input);

        if (empty(Auth()->parental)) {
            return redirect('user/profile/parental');
        } else {
            LaravelSweetAlert::setMessageSuccess('บันทึกข้อมูลสำเร็จ');
            return redirect()->back();
        }
    }

    public function Parental()
    {
        return view('user.profile.parental');
    }

    public function storeParental(
        Request $request
    )
    {
        Parental::updateOrCreate($request->except('_token'));

        if (empty(Auth()->child)) {
            return redirect('user/profile/child');
        } else {
            LaravelSweetAlert::setMessageSuccess('บันทึกข้อมูลสำเร็จ');
            return redirect()->back();
        }
    }


    public function Disabled()
    {
        return view('user.profile.disabled');

    }

    public function storeDisabled(
        Request $request
    )
    {
        Disabled::updateOrCreate($request->except('_token'));
        LaravelSweetAlert::setMessageSuccess('บันทึกข้อมูลสำเร็จ');
        return redirect()->back();
    }
}