<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Taxpayer extends Model
    {
        protected $guarded = [
            'id',
        ];

        public function Parent()
        {
            return $this->hasMany('App\Parent');
        }

        public function Child()
        {
            return $this->hasMany('App\Child');
        }

        public function Spouse()
        {
            return $this->hasOne('App\Spouse');
        }

        public function Disability()
        {
            return $this->hasOne('App\Disability');
        }

    }
