<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSpousesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('spouses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('pre_name');
                $table->string('first_name');
                $table->string('last_name');
                $table->date('birth_date');
                $table->string('id_card');
                $table->integer('type')->default(0);
                $table->integer('taxpayer_id')->unsigned();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('spouses');
        }
    }
