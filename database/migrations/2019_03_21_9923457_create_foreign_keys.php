<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Eloquent\Model;

    class CreateForeignKeys extends Migration
    {

        public function up()
        {
/*            Schema::table('children', function (Blueprint $table) {
                $table->foreign('taxpayer_id')->references('id')->on('taxpayers')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });*/
            Schema::table('parentals', function (Blueprint $table) {
                $table->foreign('taxpayer_id')->references('id')->on('taxpayers')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
            Schema::table('spouses', function (Blueprint $table) {
                $table->foreign('taxpayer_id')->references('id')->on('taxpayers')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
            Schema::table('disableds', function (Blueprint $table) {
                $table->foreign('taxpayer_id')->references('id')->on('taxpayers')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });
            Schema::table('taxpayers', function (Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            });

//            Schema::table('datas', function (Blueprint $table) {
//                $table->foreign('id')->references('id')->on('users')
//                    ->onDelete('cascade')
//                    ->onUpdate('cascade');
//            });


        }

        public function down()
        {
            Schema::table('childs', function (Blueprint $table) {
                $table->dropForeign('childs_user_id_foreign');
            });
            Schema::table('parents', function (Blueprint $table) {
                $table->dropForeign('parents_user_id_foreign');
            });
            Schema::table('couples', function (Blueprint $table) {
                $table->dropForeign('couples_user_id_foreign');
            });
/*            Schema::table('disableds', function (Blueprint $table) {
                $table->dropForeign('disableds_user_id_foreign');
            });*/
        }
    }