<?php


Route::get('/export', 'User\ExportController@exportPDF');
Route::get('/tx', 'TestController@index');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear', function () {
    Session::flush();
});
Route::get('/bypass', 'HomeController@bypass');


Route::get('/calculate/{value}', 'User\CalculateController@calculate');
Route::get('/', 'HomeController@index')->name('index');
Route::get('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');


Route::get('/register', 'Auth\RegisterController@register');
Route::post('/register', 'Auth\RegisterController@create');

Route::get('password/reset', 'Auth\ForgotPasswordController@ShowLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@SendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@ShowResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


/*    Route::group(['prefix' => 'tax', 'as' => 'tax.'], function () {
        Route::get('/', function () {
            return view('tax.index');
        });

        Route::get('/step1', function () {
            return view('tax.step1');
        });

        Route::get('/step2', function () {
            return view('tax.step2');
        })->name('step2');
        Route::get('/step3', function () {
            return view('tax.step3');
        })->name('step3');
        Route::get('/step4', function () {
            return view('tax.step4');
        })->name('step4');

        Route::get('/step5', function () {
            return view('tax.step5');
        })->name('step5');

        Route::get('/step6', function () {
            return view('tax.step6');
        })->name('step6');
        Route::post('step1', 'TaxController@step1')->name('step1');
        Route::post('step2', 'TaxController@step2')->name('step2');
        Route::post('step3', 'TaxController@step3')->name('step3');
        Route::post('step4', 'TaxController@step4')->name('step4');
        Route::post('step5', 'TaxController@step5')->name('step5');
        Route::post('step6', 'TaxController@step6')->name('step6');
        Route::post('user/profile', 'TaxController@step6')->name('step6');


    });*/


//    Route::get('logout', 'A');


Auth::routes();


Route::get('/user/profile/taxpayer', 'User\ProfileController@TaxPayer');
Route::post('/user/profile/taxpayer', 'User\ProfileController@StoreTaxPayer');
Route::get('/user/profile/spouse', 'User\ProfileController@spouse');
Route::post('/user/profile/spouse', 'User\ProfileController@storeSpouse');
Route::get('/user/profile/parental', 'User\ProfileController@parental');

Route::get('/user/profile/child', 'User\Profile\ChildController@index');
Route::get('/user/profile/child/create', 'User\Profile\ChildController@create');
Route::post('/user/profile/child/create', 'User\Profile\ChildController@store');
Route::get('/user/profile/child/{id}', 'User\Profile\ChildController@edit');
Route::put('/user/profile/child/{id}', 'User\Profile\ChildController@update');
Route::get('/user/profile/child/{id}/delete', 'User\Profile\ChildController@delete');


Route::get('/user/profile/disabled', 'User\Profile\DisabledController@index');
Route::get('/user/profile/disabled/create', 'User\Profile\DisabledController@create');
Route::post('/user/profile/disabled/create', 'User\Profile\DisabledController@store');
Route::get('/user/profile/disabled/{id}', 'User\Profile\DisabledController@edit');
Route::put('/user/profile/disabled/{id}', 'User\Profile\DisabledController@update');
Route::get('/user/profile/disabled/{id}/delete', 'User\Profile\DisabledController@delete');

Route::get('/user/profile/exemption', 'User\ProfileController@exemption');

Route::get('/user/tax', function () {
    return redirect('/user/tax/assessable_income');
});

Route::get('/user/tax/assessable_income', 'User\TaxController@AssessableIncome');
Route::get('/user/tax/allowance1', 'User\TaxController@Allowance1');
Route::get('/user/tax/allowance2', 'User\TaxController@allowance2');
Route::get('/user/tax/allowance3', 'User\TaxController@allowance3');
Route::get('/user/tax/allowance4', 'User\TaxController@allowance4');
Route::get('/user/tax/allowance5', 'User\TaxController@allowance5');
Route::get('/user/tax/exemption', 'User\TaxController@exemption');
Route::get('/user/tax/summary', 'User\TaxController@exemption');


Route::post('/user/tax/assessable_income', 'User\TaxController@storeAssessableIncome');
Route::post('/user/tax/allowance1', 'User\TaxController@StoreAllowance1');
Route::post('/user/tax/allowance2', 'User\TaxController@StoreAllowance2');
Route::post('/user/tax/allowance3', 'User\TaxController@StoreAllowance3');
Route::post('/user/tax/allowance4', 'User\TaxController@StoreAllowance4');
Route::post('/user/tax/allowance5', 'User\TaxController@StoreAllowance5');
Route::post('/user/tax/exemption', 'User\TaxController@StoreExemption');
Route::get('/user/tax/calculate', 'User\CalculateController@calculate');

